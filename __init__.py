from .to_sum_of_squares import *
from .new_diap import *
__all__ = ["to_sum_of_squares", "test_sum", "new_diap"]
