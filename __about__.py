__all__ = ['__title__', '__version__', '__author__', '__email__', '__copyright__',]
__title__ = 'lagrang_pack'
__version__ = 1.1
__author__ = 'Svetlana Ilina'
__email__ = 'skammm.04@mail.ru'
__copyright__ = f'Copyright (c) 2022, {__author__}'
